import React, {useState} from 'react'

export const TodoForm = ({ addTodo }) => {
  const [value, setValue] = useState('');
  const [errors, setErrors] = useState('');
  const [touched, setTouched] = useState(false);

  const validate = (value) => {
    let errors = '';
    if (value.length < 1 || value.charAt(0) === ' ') {
      errors = 'The input must start with an alphanumeric character and cannot be empty';
    }
    return errors;
  }

  const handleBlur = () => {
    setTouched(false);
    setErrors('');
  }

  const handleChange = (e) => {
    setValue(e.target.value);
    setTouched(true);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const newErrors = validate(value);
    setErrors(newErrors);
    if (newErrors.length === 0) {
      addTodo(value);
      setValue('');
    }
  };

  return (
    <form onSubmit={handleSubmit} className="TodoForm">
      <h4 style={{ color: 'red' }}>{errors}</h4>
      <input type="text" value={value} onChange={handleChange} onBlur={handleBlur}
        className="todo-input" placeholder='What is the task for today?' />
      <button type="submit" className='todo-btn'>Add Task</button>
  </form>
  )
}